{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad (unless)
import Control.Concurrent (threadDelay)

import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as M

import Data.List (foldl')

import Foreign.C.Types (CInt (..) )

import SDL
import SDL.Time (time, delay)
import Linear (V4(..))

import TextureMap (TextureMap, TextureId (..))
import qualified TextureMap as TM
import qualified Entite as E
import Sprite (Sprite)
import qualified Sprite as S
import Environnement (Envi)
import qualified Environnement as EV
import Carte (Carte(..))
import qualified Carte as CR

import SpriteMap (SpriteMap, SpriteId (..))
import qualified SpriteMap as SM

import Keyboard (Keyboard)
import qualified Keyboard as K

import qualified Debug.Trace as T

import Model (Modele( Cont ))
import qualified Model as MD

loadBackground :: Renderer-> FilePath -> TextureMap -> SpriteMap -> String -> IO (TextureMap, SpriteMap)
loadBackground rdr path tmap smap  id = do
  tmap' <- TM.loadTexture rdr path (TextureId id ) tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId id) (S.mkArea 0 0 490 630)
  let smap' = SM.addSprite (SpriteId id) sprite smap
  return (tmap', smap')

loadPerso :: Renderer-> FilePath -> TextureMap -> SpriteMap -> String -> IO (TextureMap, SpriteMap)
loadPerso rdr path tmap smap id = do
  tmap' <- TM.loadTexture rdr path (TextureId id) tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId id) (S.mkArea 0 0 70 70)
  let smap' = SM.addSprite (SpriteId id) sprite smap
  return (tmap', smap')
chargerEntites :: Renderer-> [E.Entite] -> TextureMap ->  SpriteMap -> IO (TextureMap, SpriteMap)
chargerEntites r (e :[]) tmap smap = do 
                                (tmap', smap') <-loadPerso r (E.g e) tmap smap ("entite" ++ show(E.getIdEnt e))
                                return (tmap', smap')
chargerEntites r (e : es) tmap smap = do 
                                    (tmap', smap') <- loadPerso r (E.g e) tmap smap ("entite" ++ show(E.getIdEnt e))
                                         
                                    (tmap2, smap2) <- chargerEntites r es tmap' smap' 
                                    return (tmap2, smap2)
loadObj :: Renderer-> FilePath -> TextureMap -> SpriteMap -> String -> IO (TextureMap, SpriteMap)
loadObj rdr path tmap smap id = do
  tmap' <- TM.loadTexture rdr path (TextureId id ) tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId id ) (S.mkArea 0 0 70 70)
  let smap' = SM.addSprite (SpriteId id ) sprite smap
  return (tmap', smap')

chargerLaby :: Renderer-> [CR.Case] -> TextureMap ->  SpriteMap -> IO (TextureMap, SpriteMap)
chargerLaby r (c : []) tmap smap = do 
                                    (tmap', smap') <-loadObj r (CR.f c) tmap smap "obj62"
                                    return (tmap', smap')
chargerLaby r (c : cs ) tmap smap = do 
                                    (tmap', smap') <- loadObj r (CR.f c) tmap smap ("obj" ++ show(62 - length cs))
                                         
                                    (tmap2, smap2) <- chargerLaby r cs tmap' smap' 
                                    return (tmap2, smap2)


displayLaby :: Renderer ->[CR.Coord] -> TextureMap -> SpriteMap -> IO ()
displayLaby renderer ((CR.C x y) : []) tmap smap = do 
                                                  S.displaySprite renderer tmap (S.moveTo (SM.fetchSprite (SpriteId (getId x y)) smap)
                                                                                  (fromIntegral(70 * y))
                                                                                  (fromIntegral(70 * x)))
displayLaby renderer ((CR.C x y) : l ) tmap smap = do 
                                                  S.displaySprite renderer tmap (S.moveTo (SM.fetchSprite (SpriteId (getId x y)) smap)
                                                                                 (fromIntegral(70 * y))
                                                                                  (fromIntegral(70 * x)))
                                                  displayLaby renderer l tmap smap 

                                              


displayEnt ::  Renderer -> CR.Coord -> String ->  TextureMap -> SpriteMap -> IO ()
displayEnt renderer (CR.C x y) l tmap smap = do 
                                                S.displaySprite renderer tmap (S.moveTo (SM.fetchSprite (SpriteId l) smap)
                                                                    (fromIntegral(70 * y))
                                                                   (fromIntegral(70 * x)))

getId :: Int -> Int -> String
getId x y = let id =  ((7 * x) +  y)
                in "obj"++ show(id)

main :: IO ()
main = do
  initializeAll
  window <- createWindow "Minijeu" $ defaultWindow { windowInitialSize = V2 490 630 }
  renderer <- createRenderer window (-1) defaultRenderer
   -- initialisation de l'état du clavier
  let kbd = K.createKeyboard
    -- initialisation de l'état du jeu
  let game = MD.init 
  -- chargement de l'image du fond
  (tmap, smap) <- loadBackground renderer "assets/background.jpg" TM.createTextureMap SM.createSpriteMap "background"
 
  let crt = MD.getLab game
  -- (CR.getLaby crt) 
  let l= CR.toListeCase crt
  (tm, sm) <- chargerLaby renderer l tmap smap
  let le = EV.getListEnt(MD.getEnv game)
  (t,s) <- chargerEntites renderer le tm sm
  --(t', s') <- loadBackground renderer "assets/win.jpg" tmap smap "win"
 
  -- lancement de la gameLoop
  gameLoop 60 renderer t s kbd game 0

gameLoop :: (RealFrac a, Show a) => a -> Renderer -> TextureMap -> SpriteMap -> Keyboard -> Modele -> Int -> IO ()
gameLoop frameRate renderer tmap smap kbd game t = do
  startTime <- time
  events <- pollEvents
 
  let kbd' = K.handleEvents events kbd
  let newkbd = K.createKeyboard
  clear renderer
           
  
  if (MD.gameWin game)
     then  do
            
            (tmap', smap') <- loadBackground renderer "assets/win.jpg" TM.createTextureMap SM.createSpriteMap "win"
            S.displaySprite renderer tmap' (SM.fetchSprite (SpriteId "win") smap') 
            
            present renderer

            threadDelay 2000000 
            -- Fin du Jeu --
      else if ( MD.gameLose game )
            then do      
                  (tmap', smap') <- loadBackground renderer "assets/lose.png" TM.createTextureMap SM.createSpriteMap "lose"
                  S.displaySprite renderer tmap' (SM.fetchSprite (SpriteId "lose") smap') 
                    
                  present renderer

                  threadDelay 2000000   
      else  do
            -- veuillez décommenter la ligne suivante pour tester le show de carte 
            --CR.writeCarteInFile (MD.getLab game) "app/save.txt"
            S.displaySprite renderer tmap (SM.fetchSprite (SpriteId "background") smap)
                  ---  dispalay laby 
            let ls = CR.toListeCord (MD.getLab game)
            displayLaby renderer ls tmap smap
                    --- display entities
            
            
                        
            displayEnt renderer (EV.getPos 0 (MD.getEnv game)) "entite0" tmap smap
            displayEnt renderer (EV.getPos 1 (MD.getEnv game)) "entite1" tmap smap
            displayEnt renderer (EV.getPos 2 (MD.getEnv game)) "entite2" tmap smap
           

            present renderer

            threadDelay 50000
            let game'' = MD.moveMonster t 2 game 
            let game' = MD.moveMonster t 1 game''
            --gameLoop frameRate renderer tmap smap newkbd gsU
             -- lecture du clavier --    
            let tour = t + 1
            unless (K.keypressed KeycodeX kbd') (if (K.keypressed KeycodeQ kbd') 
                                                              then let gsL = MD.moveLeft game' 0
                                                                      in  gameLoop frameRate renderer tmap smap newkbd gsL tour
                                                              else if(K.keypressed KeycodeD kbd') 
                                                                        then let gsR = MD.moveRight game' 0
                                                                                in gameLoop frameRate renderer tmap smap newkbd gsR tour
                                                                        else if(K.keypressed KeycodeZ kbd') 
                                                                                  then let gsU = MD.moveUp game' 0 
                                                                                          in gameLoop frameRate renderer tmap smap newkbd gsU tour
                                                                                  else if(K.keypressed KeycodeS kbd') 
                                                                                          then let gsD = MD.moveDown game' 0
                                                                                                  in gameLoop frameRate renderer tmap smap newkbd gsD tour
                                                               else gameLoop frameRate renderer tmap smap kbd' game' tour
                                                  )
                            
                      
              
            


                          