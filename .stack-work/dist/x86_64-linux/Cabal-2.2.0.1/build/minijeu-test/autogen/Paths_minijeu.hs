{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_minijeu (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/bin"
libdir     = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/lib/x86_64-linux-ghc-8.4.4/minijeu-0.1.0.0-EMmP1oQPmJ2eiyhauvkJC-minijeu-test"
dynlibdir  = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/lib/x86_64-linux-ghc-8.4.4"
datadir    = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/share/x86_64-linux-ghc-8.4.4/minijeu-0.1.0.0"
libexecdir = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/libexec/x86_64-linux-ghc-8.4.4/minijeu-0.1.0.0"
sysconfdir = "/home/heythem/Documents/projet-paf-2020-nouari-hemaizi/.stack-work/install/x86_64-linux/1f9f9d25235295985a24eea9c0a332adccac2a3ade0d220fb040344b4942c8a5/8.4.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "minijeu_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "minijeu_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "minijeu_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "minijeu_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "minijeu_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "minijeu_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
