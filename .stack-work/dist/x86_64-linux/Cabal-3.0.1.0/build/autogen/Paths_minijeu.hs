{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_minijeu (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/bin"
libdir     = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/lib/x86_64-linux-ghc-8.8.3/minijeu-0.1.0.0-DPwroyLJ3e9Ia9Wk8bqCgM"
dynlibdir  = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/lib/x86_64-linux-ghc-8.8.3"
datadir    = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/share/x86_64-linux-ghc-8.8.3/minijeu-0.1.0.0"
libexecdir = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/libexec/x86_64-linux-ghc-8.8.3/minijeu-0.1.0.0"
sysconfdir = "/home/heythem/Documents/projects/mini-jeu/.stack-work/install/x86_64-linux/c4d0fc03fd2ce4234b72d0a9fba3eb23e67be33dc4031cf59864e11a06c68edb/8.8.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "minijeu_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "minijeu_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "minijeu_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "minijeu_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "minijeu_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "minijeu_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
