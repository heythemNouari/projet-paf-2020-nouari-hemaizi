import Test.Hspec
import CarteSpec as C
import EntiteSpec as E
import EnvironnementSpec as EN
import ModelSpec as M

main :: IO ()
main = hspec $ do
  C.cSpec
  E.eSpec
  EN.enSpec
  M.mSpec

