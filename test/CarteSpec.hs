
-- BINOME ----------------------
-- Hemaizi camilia -------------
-- Nouari Heythem --------------
--------------------------------

module CarteSpec where

import Carte

import Test.Hspec
import Test.QuickCheck
import qualified Data.Map.Strict as M

carte = initCarte 

cartes = Carte 1 3 (M.fromList [ ((C 0 0),Mur),((C 0 1),Mur ),((C 0 2),Mur)])

addLinesSpec = do
  describe "addLines" $ do

    it "returns the file representation of our Map" $ do
      addLines 3 "M N M M M N"
        `shouldBe` ("M N \nM M M N\n")



getCaseSpec = do
  describe "getCase" $ do

    it "returns the case of a card at the given coord  " $ do
      getCase carte (C 0 0)
        `shouldBe` (Mur)

    it "returns the case of a card at the given coord  " $ do
      getCase carte (C 8 1)
        `shouldBe` (Entree)

    it "returns the case of a card at the given coord  " $ do
      getCase carte (C 1 6)
        `shouldBe` (Sortie)
    
    it "returns the case of a card at the given coord  " $ do
      getCase carte (C 5 3)
        `shouldBe` (Carte.Normal)


franchissable_caseSpec = do
  describe "franchissable_case" $ do

    it "returns   " $ do
      franchissable_case carte (C 0 0)
        `shouldBe` (False)

    it "returns   " $ do
      franchissable_case carte (C 4 1)
        `shouldBe` (True)


getLabySpec = do
  describe "getLaby" $ do

    it "returns  labyrinth " $ do
      getLaby carte 
        `shouldBe` (initLaby)



toListeCaseSpec = do
  describe "toListeCase" $ do

    it "returns  list of cases " $ do
      toListeCase cartes 
        `shouldBe` ([Mur,Mur,Mur])


toListeCordSpec = do
  describe "toListeCord" $ do

    it "returns  list of coords " $ do
      toListeCord cartes 
        `shouldBe` ([(C 0 0), (C 0 1), (C 0 2)])
initLaby_postSpec = do
  describe "initLaby_post" $ do

    it "returns the case of a card at the given coord  " $ do
      initLaby_post initLaby 
        `shouldBe` (True)
getCase_preSpec = do
  describe "getCase_pre" $ do

    it "returns the case of a card at the given coord  " $ do
      getCase_pre carte (C 10 11) 
        `shouldBe` (False)
    it "returns the case of a card at the given coord  " $ do
      getCase_pre carte (C 5 5) 
        `shouldBe` (True)
getLaby_postSpec = do
  describe "getLaby_post" $ do

    it "returns the case of a card at the given coord  " $ do
      getLaby_post carte 
        `shouldBe` (True)
toListeCase_postSpec = do
  describe "toListeCase_post" $ do

    it "returns the case of a card at the given coord  " $ do
      toListeCase_post carte 
        `shouldBe` (True)


-- Exemple de générateur garantissant l'invariant
gen :: Gen Int
gen = do
    i <- choose (0, 8)  
    return $ i

------- QuickCheck ------

prop_murGaucheSpec = do
  describe "prop_murGauche" $ do
    it " returns true if all cases with y = 0 are walls  " $ property $
      forAll (oneof [return  0, return 1, return 2, return 3, return 4, return 5, return 6]) $
      \xs -> prop_murGauche carte xs 

cSpec = do
  addLinesSpec
  getCaseSpec
  franchissable_caseSpec
  getLabySpec
  toListeCaseSpec
  toListeCordSpec
  initLaby_postSpec
  getCase_preSpec
  getLaby_postSpec
  toListeCase_postSpec
  prop_murGaucheSpec