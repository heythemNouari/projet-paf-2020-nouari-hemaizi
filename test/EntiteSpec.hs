
-- BINOME ----------------------
-- Hemaizi camilia -------------
-- Nouari Heythem --------------
--------------------------------

module EntiteSpec where

import Entite

import Test.Hspec

import qualified Data.Map.Strict as M

getIdEntSpec = do
  describe "getIdEnt" $ do

    it "returns " $ do
      getIdEnt (Joueur 0)
        `shouldBe` (0)


getIdSpec = do
  describe "getId" $ do

    it "returns " $ do
      getId [(Joueur 0),(Monstre 1)]
        `shouldBe` (["entite0","entite1"])


eSpec = do
    getIdEntSpec
    getIdSpec
  
