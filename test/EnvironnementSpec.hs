
-- BINOME ----------------------
-- Hemaizi camilia -------------
-- Nouari Heythem --------------
--------------------------------

module EnvironnementSpec where

import Environnement
import Carte
import Entite

import Test.QuickCheck
import Test.Hspec

import qualified Data.Map.Strict as M

env = initEnv 

getmapSpec = do
  describe "getmap" $ do

    it "returns " $ do
      getmap env
        `shouldBe` (M.fromList [((C 8 1),[(Joueur 0)]),((C 5 1),[(Monstre 1)]),((C 7 4),[(Monstre 2 )])] )



franchissable_envSpec = do
  describe "franchissable_env" $ do

    it "returns " $ do
      franchissable_env (C 5 1 ) env
        `shouldBe` (False)

    it "returns " $ do
      franchissable_env (C 8 1) env
        `shouldBe` (True)

-------------------------
trouve_idSpec = do
  describe "trouve_id" $ do

    it "returns " $ do
      trouve_id 0 env 
        `shouldBe` (Just ((C 8 1 ), (Joueur 0)))



rm_env_idSpec = do
  describe "rm_env_id" $ do

    it "returns " $ do
      rm_env_id 1 env 
        `shouldBe` ( Envi (M.fromList [((C 8 1),[(Joueur 0)]),((C 5 1),[]),((C 7 4),[(Monstre 2 )])] ))

bouge_idSpec = do
  describe "bouge_id" $ do

    it "returns " $ do
      bouge_id 0 (C 7 5) env 
        `shouldBe` (Envi (M.fromList [((C 8 1),[]),((C 5 1),[(Monstre 1)]),((C 7 4),[(Monstre 2 )]) ,((C 7 5),[(Joueur 0 )]) ] ))


getListEntSpec = do
  describe "getListEnt" $ do

    it "returns " $ do
      getListEnt env 
        `shouldBe` ([(Monstre 1), (Monstre 2), (Joueur 0)])

getListCoordsSpec = do
  describe "getListCoords" $ do

    it "returns " $ do
      getListCoords env
        `shouldBe` ([(C 5 1),(C 7 4),(C 8 1)])

getPosSpec = do
  describe "getPos" $ do

    it "returns " $ do
      getPos 0 env 
        `shouldBe` ((C 8 1))

getPos_precSpec = do
  describe "getPos_prec" $ do

    it "returns " $ do
      getPos_prec 0 
        `shouldBe` (True)
    it "returns " $ do
      getPos_prec 1 
        `shouldBe` (True)
    it "returns " $ do
      getPos_prec 2 
        `shouldBe` (True)

------------ QuickCheck ---------------------------

prop_nbEntite_fixeSpec = do
  describe "prop_nbEntite_fixe" $ do
    it " returns true if nb Entite = 3 " $ property $
      forAll (oneof [return env
                     , return (bouge_id 1 (C 0 1) env)
                     , return (bouge_id 0 (C 2 4) env)
                     , return (bouge_id 2 (C 8 5) env)
                     ]) $
      \xs -> prop_nbEntite_fixe xs





enSpec = do
    getmapSpec
    franchissable_envSpec
    trouve_idSpec
    rm_env_idSpec
    bouge_idSpec
    getListEntSpec
    getListCoordsSpec
    getPosSpec
    prop_nbEntite_fixeSpec
    getPos_precSpec