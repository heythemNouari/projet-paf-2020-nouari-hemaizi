
-- BINOME ----------------------
-- Hemaizi camilia -------------
-- Nouari Heythem --------------
--------------------------------

module ModelSpec where

import qualified Model as MD
import Environnement
import Carte
import Entite


import Test.Hspec

import qualified Data.Map.Strict as M

model = MD.init
env' = Envi (M.fromList [((C 5 1),[(Monstre 1)]) ,((C 7 3),[(Monstre 2 )]) ,((C 7 4),[]),((C 8 1),[(Joueur 0)]) ] )

envR = Envi (M.fromList [((C 5 1),[(Monstre 1)]) ,((C 7 5),[(Monstre 2 )]) ,((C 7 4),[]),((C 8 1),[(Joueur 0)]) ] )
envD = Envi (M.fromList [((C 6 1),[(Monstre 1)]) ,((C 7 4),[(Monstre 2 )]) ,((C 5 1),[]),((C 8 1),[(Joueur 0)]) ] )

envU = Envi (M.fromList [((C 5 1),[(Monstre 1)]) ,((C 7 4),[(Monstre 2 )]) ,((C 8 1),[]),((C 7 1),[(Joueur 0)]) ] )


moveLeftSpec = do
  describe "moveLeft" $ do

    it "returns " $ do
      MD.moveLeft  model 1 
        `shouldBe` (MD.Cont (MD.getLab model) initEnv)
    it "returns " $ do
      MD.moveLeft  model 2 
        `shouldBe` (MD.Cont (MD.getLab model) env')

moveRightSpec = do
  describe "moveRight" $ do

    it "returns " $ do
      MD.moveRight  model 2 
        `shouldBe` (MD.Cont (MD.getLab model) envR)
    
moveUpSpec = do
  describe "moveUp" $ do

    it "returns " $ do
      MD.moveUp  model 0 
        `shouldBe` (MD.Cont (MD.getLab model) envU)
moveDownSpec = do
  describe "moveDown" $ do

    it "returns " $ do
      MD.moveDown  model 1 
        `shouldBe` (MD.Cont (MD.getLab model) envD)

init_postSpec = do
  describe "init_post" $ do

    it "returns " $ do
      MD.init_post  model  
        `shouldBe` (True)



mSpec = do
    moveLeftSpec
    moveRightSpec
    moveUpSpec
    moveDownSpec
    init_postSpec