module Environnement where

import SDL
import qualified Data.Map as M
import Data.List (foldl')
import Carte as Carte

import Entite (Entite (..))
import Data.Set (Set)
import qualified Data.Set as S



data Envi = Envi { contenu_envi :: M.Map Coord [Entite]} deriving (Show, Eq)



initEnv :: Envi
initEnv  =  Envi (M.fromList [((C 8 1),[(Joueur 0)]),((C 5 1),[(Monstre 1)]),((C 7 4),[(Monstre 2 )])] )


getmap :: Envi ->  M.Map Coord [Entite]
getmap (Envi cnt) = cnt

franchissable_env :: Coord -> Envi -> Bool
franchissable_env cord (Envi cnt) = if((elem (Monstre 1)(cnt M.! cord) )||(elem (Monstre 1)(cnt M.! cord)) )
                                        then False
                                        else True

trouve_id :: Int -> Envi -> Maybe (Coord, Entite)
trouve_id 0 (Envi cnt) = let l = M.foldlWithKey (\ acc cord ents -> if(elem (Joueur 0) ents) then cord : acc else acc ) [] cnt
                                in  if( null l)
                                        then Nothing
                                        else Just( (head l) , (Joueur 0))
trouve_id n (Envi cnt )= let l = M.foldlWithKey (\ acc cord ents -> if(elem (Monstre n) ents) then cord : acc else acc ) [] cnt
                                in  if( null l)
                                        then Nothing
                                        else Just( (head l) , (Monstre n))

rm_env_id :: Int -> Envi -> Envi
rm_env_id i (Envi cnt) = case (trouve_id i (Envi cnt)) of
                        Nothing -> (Envi cnt)
                        Just( c , e) -> let ls = cnt M.! c 
                                             in (Envi (M.insert c (dropWhile (== e) ls) cnt))
bouge_id :: Int -> Coord -> Envi -> Envi
bouge_id i cord (Envi cnt) = case (trouve_id i (Envi cnt)) of
                                Nothing -> (Envi cnt)
                                Just( c , e) -> if ( elem cord (getListCoords (Envi cnt)))
                                                     then let ls = cnt M.! cord
                                                                in Envi (M.insert cord (e:ls) (getmap(rm_env_id i (Envi cnt)))) 
                                                     else Envi (M.insert cord (e:[]) (getmap(rm_env_id i (Envi cnt))))
getListEnt :: Envi -> [Entite]
getListEnt (Envi cnt) = 
    M.foldlWithKey (\ acc cord ents -> acc ++ ents ) [] cnt

getListCoords :: Envi -> [Coord]
getListCoords (Envi cnt) = 
    M.foldlWithKey (\ acc cord ents -> acc ++ [cord] ) [] cnt



getPos_prec :: Int  -> Bool
getPos_prec i  = (i< 3 && i>=0 )

getPos :: Int -> Envi -> Coord
getPos x e = case (trouve_id x e) of 
                Nothing -> (C 0 0)
                Just(c,ent) -> c


prop_nbEntite_fixe :: Envi -> Bool
prop_nbEntite_fixe e = (length (getListEnt e)) == 3