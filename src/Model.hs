
module Model where

import SDL
import System.Random
import Keyboard (Keyboard)
import qualified Keyboard as K


import Carte (Carte(..))
import qualified Carte as CR

import Environnement (Envi(..))
import qualified Environnement as E
data Modele = Cont {carte :: Carte , -- carte actuelle
                    envi :: Envi  -- environnement actuel
                    } deriving (Show,Eq)

init ::  Modele
init =  Cont (CR.initCarte) (E.initEnv) 


--- on verifie que le joueur est bien a la meme Cord de l'entree au depart 
init_post :: Modele -> Bool 
init_post (Cont c e ) = case (E.trouve_id 0 e)of
                              Nothing -> False
                              Just( cord , ent) -> ((CR.getCase c cord)== CR.Entree) 

getLab :: Modele -> Carte
getLab (Cont c e ) = c 

--getLab_post :: Modele -> Bool


getEnv :: Modele -> Envi
getEnv (Cont c e ) = e 



moveLeft :: Modele -> Int -> Modele
moveLeft (Cont crt e ) i= case (E.trouve_id i e) of
                                Nothing -> (Cont crt e )
                                Just( c , ent) ->  if( ((CR.precY c)) >= 0 &&  (CR.franchissable_case crt (CR.C (CR.getX c) (CR.precY c))))
                                                        then let e' = E.bouge_id i (CR.C (CR.getX c) (CR.precY c)) e
                                                              in (Cont crt e' )
                                                        else (Cont crt e )
moveRight :: Modele -> Int -> Modele
moveRight (Cont crt e ) i = case (E.trouve_id i e) of
                                Nothing -> (Cont crt e )
                                Just( c , ent) -> let y = (CR.succY c)
                                                    in if( (y < 7) && (CR.franchissable_case crt (CR.C (CR.getX c) y)))
                                                        then let e' = E.bouge_id i (CR.C (CR.getX c) y) e
                                                              in (Cont crt e' )
                                                         else (Cont crt e )

moveUp :: Modele -> Int -> Modele
moveUp (Cont crt e ) i = case (E.trouve_id i e) of
                                Nothing -> (Cont crt e )
                                Just( c , ent) -> if( (CR.precX c) >= 0 && (CR.franchissable_case crt (CR.C (CR.precX c) (CR.getY c))))
                                                        then let e' = E.bouge_id i (CR.C (CR.precX c) (CR.getY c)) e
                                                              in (Cont crt e' )
                                                        else (Cont crt e )


moveDown :: Modele -> Int -> Modele
moveDown (Cont crt e ) i = case (E.trouve_id i e) of
                                Nothing -> (Cont crt e )
                                Just( c , ent) -> if( (CR.succX c) < 9 && (CR.franchissable_case crt (CR.C (CR.succX c) (CR.getY c))) )
                                                        then let e' = E.bouge_id i (CR.C (CR.succX c) (CR.getY c)) e
                                                              in (Cont crt e' )
                                                        else (Cont crt e )





gameWin :: Modele -> Bool
gameWin (Cont crt e) = case (E.trouve_id 0 e) of
                                Nothing -> False
                                Just( c , ent) -> let cas = (CR.getCase crt c)
                                                       in if( cas == CR.Sortie ) 
                                                             then True 
                                                             else False 


moveMonster :: Int -> Int -> Modele -> Modele
moveMonster k 1 m = case k `mod` 40 of  
                       0 -> moveDown m 1
                       20 -> moveUp m 1
                       otherwise -> m
moveMonster k 2 m = case k `mod` 40 of  
                       0 -> moveRight m 2
                       20 -> moveLeft m 2
                       otherwise -> m

      
-- gameLose :: Modele -> Bool
-- gameLose (Cont crt e) = if ((E.getPos 0 e )== (CR.C 0 0) || (E.getPos 1 e )== (CR.C 0 0) || (E.getPos 2 e )== (CR.C 0 0))
--                               then True
--                               else False

gameLose :: Modele -> Bool
gameLose (Cont crt e) = if ((E.getPos 0 e )== E.getPos 1 e || (E.getPos 0 e )== E.getPos 2 e)
                              then True 
                              else False