module Entite where 

import SDL



data Entite = Monstre {iden :: Int }
            | Joueur {iden :: Int }
            deriving (Show,Eq,Ord)



g :: Entite -> String
g e = case e of 
     Joueur  x -> "assets/pers.png"
     otherwise -> "assets/virus.bmp"

getIdEnt :: Entite -> Int
getIdEnt (Monstre x) = x
getIdEnt (Joueur x) = x


getId :: [Entite] -> [String]
getId [] = []
getId ((Joueur x) : ls) = ("entite"++(show x)):(getId ls) 
getId ((Monstre x) : ls) = ("entite"++(show x)):(getId ls)